package ru.bakhtiyarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.endpoint.Session;
import ru.bakhtiyarov.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAuthEndpoint {


    @NotNull
    @WebMethod
    void logout(
            @Nullable @WebParam(name = "session", partName = "session") Session session
    );

    @WebMethod
    Session login(
            @Nullable @WebParam(name = "login", partName = "login") String login,
            @Nullable @WebParam(name = "password", partName = "password") String password
    );

    @WebMethod
    void registry(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "email", partName = "email") final String email
    );

    @Nullable
    @WebMethod
    String getUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session

    );

    @NotNull
    @WebMethod
    void checkRole(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "roles", partName = "roles") final Role[] roles
    );

}
