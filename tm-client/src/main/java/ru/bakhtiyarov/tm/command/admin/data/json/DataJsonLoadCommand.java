package ru.bakhtiyarov.tm.command.admin.data.json;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.Session;
import ru.bakhtiyarov.tm.service.SessionService;

public final class DataJsonLoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-json-load";
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from json file.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA JSON LOAD]");
        @NotNull Session session = serviceLocator.getSessionService().getSession();
        endpointLocator.getAdminDataEndpoint().loadJson(session);
        System.out.println("[OK]");
    }

}