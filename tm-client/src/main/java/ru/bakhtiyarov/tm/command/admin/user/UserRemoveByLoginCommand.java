package ru.bakhtiyarov.tm.command.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.AdminUserEndpoint;
import ru.bakhtiyarov.tm.endpoint.Session;
import ru.bakhtiyarov.tm.endpoint.User;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public final class UserRemoveByLoginCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user in program.";
    }

    @Override
    public void execute() {
        System.out.println("[DELETE USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final AdminUserEndpoint adminUserEndpoint = endpointLocator.getAdminUserEndpoint();
        @NotNull Session session = serviceLocator.getSessionService().getSession();
        User user = adminUserEndpoint.removeUserByLogin(session, login);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}
