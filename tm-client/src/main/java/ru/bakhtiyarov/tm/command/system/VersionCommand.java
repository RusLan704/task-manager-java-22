package ru.bakhtiyarov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "Show version info.";
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.9");
    }

}