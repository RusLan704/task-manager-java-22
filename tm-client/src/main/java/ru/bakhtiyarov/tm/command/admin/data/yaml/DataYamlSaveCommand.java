package ru.bakhtiyarov.tm.command.admin.data.yaml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.Session;
import ru.bakhtiyarov.tm.service.SessionService;

public final class DataYamlSaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-yaml-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to yaml file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA YAML SAVE]");
        @NotNull Session session = serviceLocator.getSessionService().getSession();
        endpointLocator.getAdminDataEndpoint().saveYaml(session);
        System.out.println("[OK]");
    }

}
