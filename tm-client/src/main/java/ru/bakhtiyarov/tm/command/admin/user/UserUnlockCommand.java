package ru.bakhtiyarov.tm.command.admin.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.command.AbstractCommand;
import ru.bakhtiyarov.tm.endpoint.AdminUserEndpoint;
import ru.bakhtiyarov.tm.endpoint.Session;
import ru.bakhtiyarov.tm.endpoint.User;
import ru.bakhtiyarov.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "unlock-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user in program.";
    }

    @Override
    public void execute() {
        System.out.println("[UNLOCK USER]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final AdminUserEndpoint adminUserEndpoint = endpointLocator.getAdminUserEndpoint();
        @NotNull Session session = serviceLocator.getSessionService().getSession();
        User user = adminUserEndpoint.unLockUserByLogin(session, login);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

}
