package ru.bakhtiyarov.tm.exception.empty;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class EmptySessionException extends AbstractException {

    @NotNull
    public EmptySessionException() {
        super("Error! Session is empty...");
    }

}
