package ru.bakhtiyarov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.endpoint.*;
import ru.bakhtiyarov.tm.api.repository.IProjectRepository;
import ru.bakhtiyarov.tm.api.repository.ISessionRepository;
import ru.bakhtiyarov.tm.api.repository.ITaskRepository;
import ru.bakhtiyarov.tm.api.repository.IUserRepository;
import ru.bakhtiyarov.tm.api.service.*;
import ru.bakhtiyarov.tm.endpoint.*;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.repository.ProjectRepository;
import ru.bakhtiyarov.tm.repository.SessionRepository;
import ru.bakhtiyarov.tm.repository.TaskRepository;
import ru.bakhtiyarov.tm.repository.UserRepository;
import ru.bakhtiyarov.tm.service.*;

import javax.xml.ws.Endpoint;

public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ServiceLocator serviceLocator = this;

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IDomainService domainService = new DomainService(serviceLocator);

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final SessionService sessionService = new SessionService(serviceLocator, sessionRepository);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(serviceLocator);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(serviceLocator);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(serviceLocator);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(serviceLocator);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(serviceLocator);

    @NotNull
    private final IAdminDataEndpoint adminDataEndpoint = new AdminDataEndpoint(serviceLocator);

    @NotNull
    private final IAdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(serviceLocator);

    public void run(@Nullable final String... args) {
        propertyService.init();
        initUsers();
        initEndpoint();
        System.out.println("SERVER START!!!");
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void initEndpoint() {
        registryEndpoint(taskEndpoint);
        registryEndpoint(projectEndpoint);
        registryEndpoint(userEndpoint);
        registryEndpoint(adminDataEndpoint);
        registryEndpoint(adminUserEndpoint);
        registryEndpoint(sessionEndpoint);
        registryEndpoint(authEndpoint);
    }

    private void registryEndpoint(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @NotNull
    @Override
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }

    @NotNull
    @Override
    public IDomainService getDomainService() {
        return domainService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

}