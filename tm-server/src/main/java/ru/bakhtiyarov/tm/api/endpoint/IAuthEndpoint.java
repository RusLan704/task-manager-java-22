package ru.bakhtiyarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.enumeration.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IAuthEndpoint {

    @WebMethod
    void registry(
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "email", partName = "email") final String email
    );

    @Nullable
    @WebMethod
    String getUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session

    );

}
