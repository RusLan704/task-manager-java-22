package ru.bakhtiyarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void addAllTasks(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "tasks", partName = "tasks") final List<Task> tasks
    );

    @WebMethod
    void addTask(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "task", partName = "task") final Task task
    );

    @WebMethod
    void createTaskByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    );

    void createTaskByNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    );

    @WebMethod
    void removeTaskByUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "task", partName = "task") final Task task
    );

    @NotNull
    @WebMethod
    List<Task> findAllTasksByUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    void clearAllTasksByUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @WebMethod
    void clearAllTasks(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @NotNull
    List<Task> finaAllTasks(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    );

    @Nullable
    @WebMethod
    Task findTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

    @Nullable
    @WebMethod
    Task findTaskOneById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

    @Nullable
    @WebMethod
    Task findTaskOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") final int index
    );

    @Nullable
    @WebMethod
    Task findTaskOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    );

    @Nullable
    @WebMethod
    Task removeTaskOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") final int index
    );

    @Nullable
    @WebMethod
    Task removeTaskOneById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

    @Nullable
    @WebMethod
    Task removeTaskOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    );

    @Nullable
    @WebMethod
    Task updateTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    );

    @Nullable
    @WebMethod
    Task updateTaskByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") final int index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    );

    @Nullable
    @WebMethod
    Task removeTaskById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

}
