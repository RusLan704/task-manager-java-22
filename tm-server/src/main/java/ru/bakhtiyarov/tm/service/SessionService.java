package ru.bakhtiyarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.ISessionRepository;
import ru.bakhtiyarov.tm.api.service.IPropertyService;
import ru.bakhtiyarov.tm.api.service.ISessionService;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;
import ru.bakhtiyarov.tm.exception.user.AccessDeniedException;
import ru.bakhtiyarov.tm.util.HashUtil;
import ru.bakhtiyarov.tm.util.SignatureUtil;

import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionService(@NotNull ServiceLocator serviceLocator, @NotNull ISessionRepository sessionRepository) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    public Session open(@Nullable String login, @Nullable String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()) throw new AccessDeniedException();
        final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return sign(session);
    }

    @Override
    public boolean checkDataAccess(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new AccessDeniedException();
        if (password == null || password.isEmpty()) throw new AccessDeniedException();
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.salt(password);
        if (passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Nullable
    @Override
    public Session sign(@Nullable Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        if (signature == null) throw new AccessDeniedException();
        session.setSignature(signature);
        return session;
    }

    public boolean isValid(@Nullable final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void validate(@Nullable Session session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
    }

    public void validate(@Nullable final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    public void signOutByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new AccessDeniedException();
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return;
        final String userId = user.getId();
        sessionRepository.removeByUserId(userId);
    }

    @Override
    public void signOutByUserId(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) return;
        sessionRepository.removeByUserId(userId);
    }

    @Override
    public void close(@Nullable Session session) {
        if (session == null) return;
        validate(session);
        removeById(session.getId());
    }

    @Override
    public void closeAll(@Nullable Session session) {
        if (session == null) return;
        validate(session);
        sessionRepository.removeByUserId(session.getUserId());
    }

    @Nullable
    @Override
    public User getUser(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        @Nullable final String userId = getUserId(session);
        if (userId == null) throw new AccessDeniedException();
        return serviceLocator.getUserService().findById(userId);
    }

    @Nullable
    @Override
    public String getUserId(@Nullable Session session) {
        validate(session);
        return session.getUserId();
    }

    @Nullable
    @Override
    public List<Session> getListSession(@Nullable Session session) {
        validate(session);
        return sessionRepository.findAll(session.getUserId());
    }

}
