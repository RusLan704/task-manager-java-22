package ru.bakhtiyarov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.repository.ISessionRepository;
import ru.bakhtiyarov.tm.entity.Session;

import java.util.ArrayList;
import java.util.List;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @Override
    public List<Session> findAll(String userId) {
        @NotNull final List<Session> result = new ArrayList<>();
        for (@Nullable final Session session : entities) {
            if (session == null) continue;
            if (userId.equals(session.getUserId()))
                result.add(session);
        }
        return result;
    }

    @Override
    public void removeByUserId(String userId) {
        @NotNull final List<Session> session = findAll(userId);
        this.entities.removeAll(session);
    }

}
