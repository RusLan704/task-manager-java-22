package ru.bakhtiyarov.tm.constant;

import org.jetbrains.annotations.NotNull;

public interface DataConst {

    @NotNull
    String FILE_BINARY = "./data.bin";

    @NotNull
    String FILE_BASE64 = "./data.base64";

    @NotNull
    String FILE_XML = ".data.xml";

    @NotNull
    String FILE_JSON = "./data.json";

    @NotNull
    String FILE_YAML = "./data.yaml";

}
