package ru.bakhtiyarov.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.exception.AbstractException;

public class IncorrectIndexException extends AbstractException {

    @NotNull
    public IncorrectIndexException(@NotNull final String value) {
        super("Error! This value ``" + value + "`` is not number...");
    }

    public IncorrectIndexException() {
        super("Error! Index is incorrect...");
    }

}