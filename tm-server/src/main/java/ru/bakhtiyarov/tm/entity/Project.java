package ru.bakhtiyarov.tm.entity;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractEntity implements Serializable {

    @NotNull
    private String name = "";

    @Nullable
    private String description = "";

    @Nullable
    private String userId;

    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}
