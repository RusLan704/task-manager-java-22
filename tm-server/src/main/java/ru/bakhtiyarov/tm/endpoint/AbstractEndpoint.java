package ru.bakhtiyarov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;

public class AbstractEndpoint {

    @NotNull
    protected final ServiceLocator serviceLocator;

    @NotNull
    public AbstractEndpoint(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
