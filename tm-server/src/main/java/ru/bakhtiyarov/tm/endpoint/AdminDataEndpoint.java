package ru.bakhtiyarov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.endpoint.IAdminDataEndpoint;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.enumeration.Role;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminDataEndpoint extends AbstractEndpoint implements IAdminDataEndpoint {

    @NotNull
    public AdminDataEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    public AdminDataEndpoint() {
        super(null);
    }

    @Override
    public void loadBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadBase64();
    }

    @Override
    public void clearBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().clearBase64();
    }

    @Override
    public void saveBase64(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveBase64();
    }

    @Override
    public void clearBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().clearBinary();
    }

    @Override
    public void loadBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadBinary();
    }

    @Override
    public void saveBinary(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveBinary();
    }

    @Override
    public void clearJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().clearJson();
    }

    @Override
    public void loadJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadJson();
    }

    @Override
    public void saveJson(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveJson();
    }

    @Override
    public void clearXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().clearXml();
    }

    @Override
    public void loadXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadXml();
    }

    @Override
    public void saveXml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveXml();
    }

    @Override
    public void clearYaml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().clearYaml();
    }

    @Override
    public void loadYaml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().loadYaml();
    }

    @Override
    public void saveYaml(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws Exception {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        serviceLocator.getDomainService().saveYaml();
    }

}
