package ru.bakhtiyarov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.endpoint.IProjectEndpoint;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.entity.Project;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.exception.user.AccessDeniedException;

import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    public ProjectEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    public ProjectEndpoint() {
        super(null);
    }

    @Override
    public void addAllProjects(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "project", partName = "project") final List<Project> projects
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().addAll(projects);
    }

    @Override
    public void addProject(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "project", partName = "project") final Project project
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        String userId = session.getUserId();
        if (userId == null) return;
        serviceLocator.getProjectService().add(session.getUserId(), project);
    }

    @Override
    public void createProjectByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().create(session.getUserId(), name);
    }

    @Override
    public void createProjectByNameDescription(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().create(session.getUserId(), name, description);
    }

    @Override
    public void clearAllProjectsByUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clearAll(session.getUserId());
    }

    @Override
    public void clearAllProjects(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().clearAll();
    }

    @NotNull
    @Override
    public List<Project> findAllProjects(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll();
    }

    @Override
    public @Nullable Project findProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findById(id);
    }

    @Override
    public @Nullable Project findProjectOneById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneById(session.getUserId(), id);
    }

    @NotNull
    @Override
    public List<Project> findAllProjectsByUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAll(session.getUserId());
    }

    @Override
    public @Nullable Project findProjectOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") final int index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByIndex(session.getUserId(), index);
    }

    @Override
    public @Nullable Project findProjectOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOneByName(session.getUserId(), name);
    }

    @Override
    public @Nullable Project removeProjectOneByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") final int index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneByIndex(session.getUserId(), index);
    }

    @Override
    public @Nullable Project removeProjectOneById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneById(session.getUserId(), id);
    }

    @Override
    public @Nullable Project removeProjectOneByName(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "name", partName = "name") final String name
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeOneByName(session.getUserId(), name);
    }

    @Override
    public @Nullable Project removeProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().removeById(id);
    }

    @Override
    public void removeProjectByUserId(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "project", partName = "project") final Project project
    ) throws AccessDeniedException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().remove(session.getUserId(), project);
    }

    @Override
    public @Nullable Project updateProjectById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateProjectById(session.getUserId(), id, name, description);
    }

    @Override
    public @Nullable Project updateProjectByIndex(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @WebParam(name = "index", partName = "index") final int index,
            @Nullable @WebParam(name = "name", partName = "name") final String name,
            @Nullable @WebParam(name = "description", partName = "description") final String description
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().updateProjectByIndex(session.getUserId(), index, name, description);
    }

}
