package ru.bakhtiyarov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bakhtiyarov.tm.api.endpoint.IAdminUserEndpoint;
import ru.bakhtiyarov.tm.api.service.ServiceLocator;
import ru.bakhtiyarov.tm.entity.Session;
import ru.bakhtiyarov.tm.entity.User;
import ru.bakhtiyarov.tm.enumeration.Role;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    @NotNull
    public AdminUserEndpoint() {
        super(null);
    }

    @NotNull
    public AdminUserEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    public @Nullable User lockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().lockUserByLogin(login);
    }

    @Override
    public @Nullable User unLockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") String login
    ) {
        serviceLocator.getSessionService().validate(session, Role.ADMIN);
        return serviceLocator.getUserService().unLockUserByLogin(login);
    }

    @Nullable
    @Override
    public User removeUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().removeByLogin(login);
    }

    @Nullable
    @Override
    public User removeUserById(
            @Nullable @WebParam(name = "session", partName = "session") final Session session,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().removeById(id);
    }

}
